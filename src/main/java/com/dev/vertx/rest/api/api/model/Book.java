package com.dev.vertx.rest.api.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Book {
    @JsonProperty(value = "id")
    private int id;

    @JsonProperty(value = "author")
    private String author;

    @JsonProperty(value = "country")
    private String country;

    @JsonProperty(value = "image_link")
    private String imageLink;

    @JsonProperty(value = "language")
    private String language;

    @JsonProperty(value = "link")
    private String link;

    @JsonProperty(value = "pages")
    private Integer pages;

    @JsonProperty(value = "title")
    private String title;

    @JsonProperty(value = "year")
    private Integer year;
}
