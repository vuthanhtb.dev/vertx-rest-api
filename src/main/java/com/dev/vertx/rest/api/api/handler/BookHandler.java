package com.dev.vertx.rest.api.api.handler;

import com.dev.vertx.rest.api.api.model.Book;
import com.dev.vertx.rest.api.api.response.BookGetAllResponse;
import com.dev.vertx.rest.api.api.response.BookGetByIdResponse;
import com.dev.vertx.rest.api.api.service.BookService;
import com.dev.vertx.rest.api.utils.ResponseUtils;
import io.vertx.core.Future;
import io.vertx.ext.web.RoutingContext;

import static com.dev.vertx.rest.api.api.constant.Params.*;

public class BookHandler {
    private final BookService bookService;

    public BookHandler(BookService bookService) {
        this.bookService = bookService;
    }

    public Future<BookGetAllResponse> getAll(RoutingContext rc) {
        final String page = rc.queryParams().get(PAGE_PARAMETER);
        final String limit = rc.queryParams().get(LIMIT_PARAMETER);

        return bookService.getAll(page, limit)
                .onSuccess(success -> ResponseUtils.buildOkResponse(rc, success))
                .onFailure(throwable -> ResponseUtils.buildErrorResponse(rc, throwable));
    }

    public Future<BookGetByIdResponse> findById(RoutingContext rc) {
        final String id = rc.pathParam(ID_PARAMETER);

        return bookService.findById(Integer.parseInt(id))
                .onSuccess(success -> ResponseUtils.buildOkResponse(rc, success))
                .onFailure(throwable -> ResponseUtils.buildErrorResponse(rc, throwable));
    }

    public Future<BookGetByIdResponse> insert(RoutingContext rc) {
        final Book book = rc.body().asJsonObject().mapTo(Book.class);

        return bookService.insert(book)
                .onSuccess(success -> ResponseUtils.buildCreatedResponse(rc, success))
                .onFailure(throwable -> ResponseUtils.buildErrorResponse(rc, throwable));
    }

    public Future<BookGetByIdResponse> update(RoutingContext rc) {
        final String id = rc.pathParam(ID_PARAMETER);
        final Book book = rc.body().asJsonObject().mapTo(Book.class);


        return bookService.update(Integer.parseInt(id), book)
                .onSuccess(success -> ResponseUtils.buildOkResponse(rc, success))
                .onFailure(throwable -> ResponseUtils.buildErrorResponse(rc, throwable));
    }

    public Future<Void> delete(RoutingContext rc) {
        final String id = rc.pathParam(ID_PARAMETER);

        return bookService.delete(Integer.parseInt(id))
                .onSuccess(success -> ResponseUtils.buildNoContentResponse(rc))
                .onFailure(throwable -> ResponseUtils.buildErrorResponse(rc, throwable));
    }
}
