package com.dev.vertx.rest.api.api.response;

import com.dev.vertx.rest.api.api.model.Book;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BookGetByIdResponse {
    @JsonProperty(value = "id")
    private final int id;

    @JsonProperty(value = "author")
    private final String author;

    @JsonProperty(value = "country")
    private final String country;

    @JsonProperty(value = "image_link")
    private final String imageLink;

    @JsonProperty(value = "language")
    private final String language;

    @JsonProperty(value = "link")
    private final String link;

    @JsonProperty(value = "pages")
    private final Integer pages;

    @JsonProperty(value = "title")
    private final String title;

    @JsonProperty(value = "year")
    private final Integer year;

    public BookGetByIdResponse(Book book) {
        this.id = book.getId();
        this.author = book.getAuthor();
        this.country = book.getCountry();
        this.imageLink = book.getImageLink();
        this.language = book.getLanguage();
        this.link = book.getLink();
        this.pages = book.getPages();
        this.title = book.getTitle();
        this.year = book.getYear();
    }
}
