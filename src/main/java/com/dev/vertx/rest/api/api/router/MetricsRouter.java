package com.dev.vertx.rest.api.api.router;

import io.vertx.ext.web.Router;
import io.vertx.micrometer.PrometheusScrapingHandler;

public class MetricsRouter {
    public static void setRouter(Router router) {
        router.route("/metrics").handler(PrometheusScrapingHandler.create());
    }
}
