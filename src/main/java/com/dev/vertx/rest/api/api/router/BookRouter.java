package com.dev.vertx.rest.api.api.router;

import com.dev.vertx.rest.api.api.handler.BookHandler;
import com.dev.vertx.rest.api.api.handler.BookValidationHandler;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.LoggerFormat;
import io.vertx.ext.web.handler.LoggerHandler;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BookRouter {
    private final Vertx vertx;
    private final BookHandler bookHandler;
    private final BookValidationHandler bookValidationHandler;

    public void setRouter(Router router) {
        router.mountSubRouter("/api/v1", this.buildBookRouter());
    }

    private Router buildBookRouter() {
        final Router bookRouter = Router.router(vertx);

        bookRouter.route("/books*").handler(BodyHandler.create());
        bookRouter.get("/books").handler(LoggerHandler.create(LoggerFormat.DEFAULT)).handler(bookValidationHandler.getAll()).handler(bookHandler::getAll);
        bookRouter.get("/books/:id").handler(LoggerHandler.create(LoggerFormat.DEFAULT)).handler(bookValidationHandler.findOne()).handler(bookHandler::findById);
        bookRouter.post("/books").handler(LoggerHandler.create(LoggerFormat.DEFAULT)).handler(bookValidationHandler.insert()).handler(bookHandler::insert);
        bookRouter.put("/books/:id").handler(LoggerHandler.create(LoggerFormat.DEFAULT)).handler(bookValidationHandler.update()).handler(bookHandler::update);
        bookRouter.delete("/books/:id").handler(LoggerHandler.create(LoggerFormat.DEFAULT)).handler(bookValidationHandler.delete()).handler(bookHandler::delete);

        return bookRouter;
    }
}
