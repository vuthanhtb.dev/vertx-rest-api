package com.dev.vertx.rest.api.api.constant;

public class Query {
    public static String SQL_GET_ALL = "SELECT * FROM books LIMIT #{limit} OFFSET #{offset}";
    public static String SQL_GET_BY_ID = "SELECT * FROM books WHERE id = #{id}";
    public static String SQL_INSERT = "INSERT INTO books (author, country, image_link, language, link, pages, title, year) VALUES (#{author}, #{country}, #{image_link}, #{language}, #{link}, #{pages}, #{title}, #{year}) RETURNING id";
    public static String SQL_UPDATE = "UPDATE books SET author = #{author}, country = #{country}, image_link = #{image_link}, language = #{language}, link = #{link}, pages = #{pages}, title = #{title}, year = #{year} WHERE id = #{id}";
    public static String SQL_DELETE = "DELETE FROM books WHERE id = #{id}";
    public static String SQL_COUNT = "SELECT COUNT(*) AS total FROM books";
}
