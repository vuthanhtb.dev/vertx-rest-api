package com.dev.vertx.rest.api.api.constant;

public class Params {
    public static String CONTENT_TYPE_HEADER = "Content-Type";
    public static String APPLICATION_JSON = "application/json";

    public static String ID_PARAMETER = "id";
    public static String PAGE_PARAMETER = "page";
    public static String LIMIT_PARAMETER = "limit";
}
