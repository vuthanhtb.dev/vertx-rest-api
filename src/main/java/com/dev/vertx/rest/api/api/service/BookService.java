package com.dev.vertx.rest.api.api.service;

import com.dev.vertx.rest.api.api.model.Book;
import com.dev.vertx.rest.api.api.repository.BookRepository;
import com.dev.vertx.rest.api.api.response.BookGetAllResponse;
import com.dev.vertx.rest.api.api.response.BookGetByIdResponse;
import com.dev.vertx.rest.api.utils.LogUtils;
import com.dev.vertx.rest.api.utils.QueryUtils;
import io.vertx.core.Future;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.pgclient.PgPool;

import java.util.List;
import java.util.stream.Collectors;

public class BookService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookRepository.class);

    private final PgPool dbClient;
    private final BookRepository bookRepository;

    public BookService(PgPool dbClient, BookRepository bookRepository) {
        this.dbClient = dbClient;
        this.bookRepository = bookRepository;
    }

    public Future<BookGetAllResponse> getAll(String p, String l) {
        return this.dbClient.withTransaction(
                        connection -> {
                            final int page = QueryUtils.getPage(p);
                            final int limit = QueryUtils.getLimit(l);
                            final int offset = QueryUtils.getOffset(page, limit);

                            return this.bookRepository.count(connection)
                                    .flatMap(total ->
                                            bookRepository.getAll(connection, limit, offset)
                                                    .map(result -> {
                                                        final List<BookGetByIdResponse> books = result.stream()
                                                                .map(BookGetByIdResponse::new)
                                                                .collect(Collectors.toList());

                                                        return new BookGetAllResponse(total, limit, page, books);
                                                    })
                                    );
                        })
                .onSuccess(success -> LOGGER.info(LogUtils.REGULAR_CALL_SUCCESS_MESSAGE.buildMessage("Read all books", success.getBooks())))
                .onFailure(throwable -> LOGGER.error(LogUtils.REGULAR_CALL_ERROR_MESSAGE.buildMessage("Read all books", throwable.getMessage())));
    }

    public Future<BookGetByIdResponse> findById(int id) {
        return this.dbClient.withTransaction(connection -> this.bookRepository.findById(connection, id).map(BookGetByIdResponse::new))
                .onSuccess(success -> LOGGER.info(LogUtils.REGULAR_CALL_SUCCESS_MESSAGE.buildMessage("Find a book", success)))
                .onFailure(throwable -> LOGGER.error(LogUtils.REGULAR_CALL_ERROR_MESSAGE.buildMessage("Find a book", throwable.getMessage())));
    }

    public Future<BookGetByIdResponse> insert(Book book) {
        return this.dbClient.withTransaction(connection -> this.bookRepository.insert(connection, book).map(BookGetByIdResponse::new))
                .onSuccess(success -> LOGGER.info(LogUtils.REGULAR_CALL_SUCCESS_MESSAGE.buildMessage("Create one book", success)))
                .onFailure(throwable -> LOGGER.error(LogUtils.REGULAR_CALL_ERROR_MESSAGE.buildMessage("Create one book", throwable.getMessage())));
    }

    public Future<BookGetByIdResponse> update(int id, Book book) {
        book.setId(id);
        return this.dbClient.withTransaction(connection -> this.bookRepository.update(connection, book).map(BookGetByIdResponse::new))
                .onSuccess(success -> LOGGER.info(LogUtils.REGULAR_CALL_SUCCESS_MESSAGE.buildMessage("Update one book", success)))
                .onFailure(throwable -> LOGGER.error(LogUtils.REGULAR_CALL_ERROR_MESSAGE.buildMessage("Update one book", throwable.getCause())));
    }

    public Future<Void> delete(Integer id) {
        return this.dbClient.withTransaction(connection -> this.bookRepository.delete(connection, id))
                .onSuccess(success -> LOGGER.info(LogUtils.REGULAR_CALL_SUCCESS_MESSAGE.buildMessage("Delete one book", id)))
                .onFailure(throwable -> LOGGER.error(LogUtils.REGULAR_CALL_ERROR_MESSAGE.buildMessage("Delete one book", throwable.getMessage())));
    }
}
