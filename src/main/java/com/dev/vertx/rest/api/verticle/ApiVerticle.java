package com.dev.vertx.rest.api.verticle;

import com.dev.vertx.rest.api.api.handler.BookHandler;
import com.dev.vertx.rest.api.api.handler.BookValidationHandler;
import com.dev.vertx.rest.api.api.handler.ErrorHandler;
import com.dev.vertx.rest.api.api.repository.BookRepository;
import com.dev.vertx.rest.api.api.router.BookRouter;
import com.dev.vertx.rest.api.api.router.HealthCheckRouter;
import com.dev.vertx.rest.api.api.router.MetricsRouter;
import com.dev.vertx.rest.api.api.service.BookService;
import com.dev.vertx.rest.api.utils.DbUtils;
import com.dev.vertx.rest.api.utils.LogUtils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.pgclient.PgPool;

public class ApiVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiVerticle.class);

    @Override
    public void start(Promise<Void> startPromise) {
        try {
            final PgPool dbClient = DbUtils.buildDbClient(this.vertx);

            final BookRepository bookRepository = new BookRepository();
            final BookService bookService = new BookService(dbClient, bookRepository);
            final BookHandler bookHandler = new BookHandler(bookService);
            final BookValidationHandler bookValidationHandler = new BookValidationHandler(this.vertx);
            final BookRouter bookRouter = new BookRouter(this.vertx, bookHandler, bookValidationHandler);

            final Router router = Router.router(this.vertx);
            ErrorHandler.buildHandler(router);

            HealthCheckRouter.setRouter(this.vertx, router, dbClient);
            MetricsRouter.setRouter(router);

            bookRouter.setRouter(router);

            this.buildHttpServer(this.vertx, startPromise, router);
        } catch (Exception e) {
            startPromise.fail(e.getMessage());
        }
    }

    private void buildHttpServer(Vertx vertx, Promise<Void> promise, Router router) {
        final int port = 8888;

        vertx.createHttpServer()
                .requestHandler(router)
                .listen(port, http -> {
                    if (http.succeeded()) {
                        promise.complete();
                        LOGGER.info(LogUtils.RUN_HTTP_SERVER_SUCCESS_MESSAGE.buildMessage(port));
                    } else {
                        promise.fail(http.cause());
                        LOGGER.info(LogUtils.RUN_HTTP_SERVER_ERROR_MESSAGE.buildMessage());
                    }
                });
    }
}
