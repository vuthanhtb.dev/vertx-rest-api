package com.dev.vertx.rest.api.verticle;

import com.dev.vertx.rest.api.utils.DbUtils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.Configuration;

public class MigrationVerticle extends AbstractVerticle {

    @Override
    public void start(Promise<Void> startPromise) {
        try {
            final Configuration config = DbUtils.buildMigrationsConfiguration();
            final Flyway flyway = new Flyway(config);

            flyway.migrate();

            startPromise.complete();
        } catch (Exception exception) {
            startPromise.fail(exception.getMessage());
        }
    }
}
